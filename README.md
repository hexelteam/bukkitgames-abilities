# BGAbilities #

This plugin adds some of the abilities from the old version of BukkitGames to the new one.

# License #

BGAbilites (an its source code) by ftbastler are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
For more information visit http://creativecommons.org/licenses/by-nc-sa/4.0/.